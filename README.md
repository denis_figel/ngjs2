# NgJS2

To run this app you need to fallow instructions:

    1. Clone source files to destination dir.
        - Start command "git clone git@bitbucket.org:denis_figel/ngjs2.git ."
        - Switch to needed branch (master)

    2. Install npm & node.js (sudo apt-get install npm && sudo npm cache clean -f && sudo npm install -g n && sudo n stable && npm install)
        - Node.JS needed for build process.
        - NPM needed for loading dependencies. 

	3. npm install
	
	4. npm run start
