(() => {
    let app = angular.module('app', ['ngRoute', 'ngResource']);

    app.config(($routeProvider) => {
        $routeProvider
            .when('/', {
                templateUrl: 'js/pages/home.htm',
                controller: 'AppController',
                controllerAs: 'Home'
            })
    });

    app.directive('pagination', () => {
        return {
            restrict: 'A',
            templateUrl: '/js/elements/pagination.html',
            scope: {
                paginationData: '=',
                allDataLength: '='
            },
            link: ($scope, element, attrs) => {
                $scope.setPage = (page) => {
                    $scope.paginationData.currentPage = page;
                }
            }
        }
    });

    app.service('currencyService', ['$http', '$rootScope', function($http, $rootScope) {
        this.getData = (currency) => {
          return $http.get('https://api.cryptonator.com/api/ticker/btc-usd')
              .then((response) => {
                  if(parseFloat(response.data.ticker.price) && parseFloat(currency)) {
                      if(parseFloat(response.data.ticker.price) < parseFloat(currency)) {
                          $rootScope.$broadcast('event::play_sound', {
                              price: parseFloat(response.data.ticker.price),
                              currency: parseFloat(currency)
                          });
                      }
                  }

                  return response.data;
              }, (response) => {
                  console.error("Error: ", response);
                  return response;
              });
        };
    }]);


    app.controller("AppController", ["$scope", "currencyService", "$interval", ($scope, currencyService, $interval) => {
        // properties
        $scope.currency = 5000;
        $scope.allData = [];
        $scope.allDataFinal = [];
        $scope.perTime = 10000;
        $scope.orderParams = {
            field: 'timestamp',
            direction: false
        };
        $scope.perPage = ["5", "10", "50", "100"];
        $scope.pagination = {
            totalItems: 0,
            currentPage: 1,
            perPage: $scope.perPage[0],
            numPages: 1,
            perPageOptions: $scope.perPage,
            begin: 0,
            end: $scope.perPage[0],
            pages: []
        };

        // Lister to event
        $scope.$on('event::play_sound', (e, data) => {
            console.log('EVENTS::DATA', data);

            let audio = new Audio('sounds/sound.mp3');
            audio.play();
        });

        // Get data
        $scope.getDataFnc = () => {
            return currencyService.getData($scope.currency)
                .then((res) => {
                    //$scope.allData.push(res);
                     $scope.allData.unshift(res);

                    // Set pagination
                    $scope.generatePagination();
                }, (response) => {
                    console.error("Error: ", response);
                });
        };

        // Set pagination function
        $scope.generatePagination = () => {
            $scope.pagination.numPages = Math.ceil($scope.allData.length / $scope.pagination.perPage);
            $scope.pagination.totalItems = $scope.allData.length;

            $scope.sliceData();
            return null;
        };

        // Slice pagination
        $scope.sliceData = () => {
            $scope.pagination.begin = parseInt((($scope.pagination.currentPage - 1) * $scope.pagination.perPage));
            $scope.pagination.end = $scope.pagination.begin + parseInt($scope.pagination.perPage);

            $scope.allDataFinal = $scope.allData.slice($scope.pagination.begin, $scope.pagination.end);

            let startPage, endPage;
            if ($scope.pagination.numPages <= 5) {
                startPage = 1;
                endPage = $scope.pagination.numPages;
            } else {
                if ($scope.pagination.currentPage <= 3) {
                    startPage = 1;
                    endPage = 8;
                } else if ($scope.pagination.currentPage + 3 >= $scope.pagination.numPages) {
                    startPage = $scope.pagination.numPages - 4;
                    endPage = $scope.pagination.numPages;
                } else {
                    startPage = $scope.pagination.currentPage - 4;
                    endPage = $scope.pagination.currentPage + 4;
                }
            }

            $scope.pagination.pages = $scope.range(startPage, endPage);
        };

        // Fill array
        $scope.range = (start, end) => {
            let ans = [];
            for (let i = start; i <= end; i++) {
                ans.push(i);
            }
            return ans;
        };

        // Wathcers
        $scope.$watch('pagination.currentPage', function(oldValue, newValue) {
            if(oldValue !== newValue) {
                $scope.sliceData();
            }
        });
        $scope.$watch('pagination.perPage', function(oldValue, newValue) {
            if(oldValue !== newValue) {
                $scope.generatePagination();
            }
        });

        // get all data
        $scope.getDataFnc();
        $interval(() =>  {
            $scope.getDataFnc();
        },$scope.perTime);

        // Order by field
        $scope.orderFnc = (field) => {
            $scope.orderParams = {
                field: field,
                direction: !$scope.orderParams.direction
            };
        };

        // Delete item from data
        $scope.deleteItem = (index) => {
            let confirmMes = confirm("Are you sure?");
            if(confirmMes) {
                $scope.allData.splice(index, 1);
            }
        };

        // Save Item in data
        $scope.saveItem = (index) => {
            $scope.allData[index].isEdit = false;
        };
    }]);
})()

