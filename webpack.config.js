var path = require('path');

module.exports = {
    entry: path.resolve(__dirname, 'js/app.js'),
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel'
        },
        { test: /\.html$/, loader: "html" },
        { test: /\.css$/, loader: "style!css" }
        ]
    },
    devtool: "#inline-source-map"
};